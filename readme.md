## Maven Parallel

```bash
mvn test -Dbrowser=<browser> -Denv=<environment> -DinteractiveTime=<interactive_time>
```

When parameters are not informed on the Maven command line, it assumes what is defined on the Properties class.

## Selenium GRID

### Server

```bash
java -jar selenium-server-standalone-3.141.59.jar -role hub
```

#### Node 

For the first node configured, you can omit -port parameter.

##### Chrome

```bash
java -Dwebdriver.chrome.driver="<driver_path>" -jar selenium-server-standalone-3.141.59.jar -role node -port <port>
```

##### Firefox

```bash
java -Dwebdriver.gecko.driver="<driver_path>" -jar selenium-server-standalone-3.141.59.jar -role node -port <port>
```