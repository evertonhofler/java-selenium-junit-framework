package suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import core.BaseSuite;
import tests.SearchProduct;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SearchProduct.class,
	SearchProduct.class
})

public class TestSuite extends BaseSuite {
	
}