package core;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import core.Properties.ExecutionType;

public class DriverFactory {

	private DriverFactory() {}

	private static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>() {
		@Override
		protected synchronized WebDriver initialValue() {
			return initDriver();
		}
	};

	public static WebDriver initDriver(){
		WebDriver driver = null;

		if (Properties.EXECUTION_TYPE == ExecutionType.LOCAL) {
			if (Properties.BROWSER == "chrome") {
				System.setProperty("webdriver.chrome.driver",Properties.DRIVER);
				driver = new ChromeDriver();
			}
			else
				if (Properties.BROWSER == "firefox") {
					System.setProperty("webdriver.gecko.driver",Properties.DRIVER);
					driver = new FirefoxDriver();
				}
		}

		if (Properties.EXECUTION_TYPE == ExecutionType.GRID) {
			DesiredCapabilities capabilities = null;
			if (Properties.BROWSER == "chrome") {
				capabilities = DesiredCapabilities.chrome();
			}
			else
				if (Properties.BROWSER == "firefox") {
					capabilities = DesiredCapabilities.firefox();
				}

			try {
				driver = new RemoteWebDriver(new URL(Properties.SELENIUM_GRID_SERVER), capabilities);
			} catch (MalformedURLException e) {
				System.err.println("Fail connecting to Selenium GRID!");
				e.printStackTrace();
			}
		}

		driver.manage().timeouts().implicitlyWait(Properties.IMPLICITLY_WAIT, TimeUnit.SECONDS);
		return driver;
	}

	public static WebDriver getDriver(){

		return threadDriver.get();
	}

	public static void killDriver() {
		WebDriver driver = getDriver();
		if (driver != null) {
			driver.quit();
		}
		if (threadDriver != null) {
			threadDriver.remove();
		}
	}

}