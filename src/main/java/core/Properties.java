package core;

public class Properties {

	public static String SUITE_START_TIMESTAMP;
	public static String BROWSER = "chrome";
	public static boolean CLOSE_BROWSER = true;
	public static String ENVIRONMENT = "prod";
	public static int IMPLICITLY_WAIT = 60;
	public static long INTERACTIVE_TIME = 0;
	public static String DRIVER;
	public static ExecutionType EXECUTION_TYPE = ExecutionType.LOCAL;
	public static String SELENIUM_GRID_SERVER = "http://10.184.100.185:4444/wd/hub";

	public static String[][] DRIVERS = {
			{"Windows",  "chrome",  "chromedriver_2.44.exe"},
			{"Windows",  "firefox", "geckodriver-v0.23.0.exe"},
			{"Mac OS X", "chrome",  "chromedriver"},
			{"Mac OS X", "firefox", "geckodriver"},
	};

	public static enum ExecutionType {
		LOCAL,
		GRID
	}

	public static String getURL(){
		String url = null;
		if (ENVIRONMENT.equals("dev"))
			url = "";
		else
			if (ENVIRONMENT.equals("qa"))
				url = "";
			else
				if (ENVIRONMENT.equals("pp"))
					url = "";
				else
					if (ENVIRONMENT.equals("prod"))
						url = "https://www.amazon.com/";
		return url;
	}

	public static void interactiveWait(){
		try{
			Thread.sleep(INTERACTIVE_TIME);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}