package core;

import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;

import core.Properties;
import pages.HomePage;
import pages.SearchResultsPage;
import support.Utilities;

public class BaseTest {
	
    protected HomePage homePage = new HomePage();
    protected SearchResultsPage searchResultsPage = new SearchResultsPage();

    @Before
    public void initializeTest() {

    	getDriver().get(Properties.getURL());
    }

    @Rule
    public TestName testName = new TestName();

    @After
    public void finalizeTest() throws IOException {
        Utilities.takeScreenshot(Utilities.getCurrentLocalDateTimeStamp() + "_" + 
        		Properties.BROWSER + "_" + testName.getMethodName());
        if (Properties.CLOSE_BROWSER) {
            killDriver();
        }
    }

}