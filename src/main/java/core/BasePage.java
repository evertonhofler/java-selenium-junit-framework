package core;

import static core.DriverFactory.getDriver;

import org.openqa.selenium.By;

import support.Utilities;

public class BasePage {

    public void type(String idElement, String text) {

        getDriver().findElement(By.id(idElement)).sendKeys(text);
    }

    public void click(String idElement) {

        getDriver().findElement(By.id(idElement)).click();
    }

    public void click(By by) {

        getDriver().findElement(by).click();
    }

    public String getText(By by) {
    	Utilities.highlightField(by);
        return getDriver().findElement(by).getText().trim();
    }

}