package core;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import support.Utilities;

public class BaseSuite {

	@BeforeClass
	public static void initializeSuite() {

		String os = System.getProperty("os.name");
		String browser = System.getProperty("browser");
		String environment = System.getProperty("env");
		String interactiveTime = System.getProperty("interactiveTime");

		Properties.SUITE_START_TIMESTAMP = Utilities.getCurrentLocalDateTimeStamp();

		File screenshotDir = new File(new File(new File(new File("").getAbsolutePath(), 
				"target"), "screenshot"), Properties.SUITE_START_TIMESTAMP);

		screenshotDir.mkdirs();

		if (browser == null) {
			browser = Properties.BROWSER;
		}	

		for (int i = 0; i < Properties.DRIVERS.length; i++)
			if (os.contains(Properties.DRIVERS[i][0].toString()))
				if (browser.contains(Properties.DRIVERS[i][1].toString())) {
					Properties.DRIVER = Properties.DRIVERS[i][2];
					break;
				}

		if (environment != null) {
			Properties.ENVIRONMENT = environment;
		}

		if (interactiveTime != null) {
			Properties.INTERACTIVE_TIME = Long.parseLong(interactiveTime);
		}
	}

	@AfterClass
	public static void finalizeSuite() {
		if (!Properties.CLOSE_BROWSER) {
			DriverFactory.killDriver();
		}
	}

}