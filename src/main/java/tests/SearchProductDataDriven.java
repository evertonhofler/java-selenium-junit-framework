package tests;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import core.BaseTest;

@RunWith(Parameterized.class)
public class SearchProductDataDriven extends BaseTest {

	@Parameter(value = 0)
	public String searchKeywords;

	@Parameters
	public static Collection<Object[]> getSearchKeywords() {
		return Arrays.asList(new Object[][] {
			{"noise cancelling headphones"},
			{"polo shirt"},
			{"stock market investing book"}
		});
	}

	@Test
	public void searchProduct() {
		homePage.typeSearchTextBox(searchKeywords);
		homePage.clickSubmit();
		assertThat(searchResultsPage.getResultsFor(), containsString(searchKeywords));
	}

}