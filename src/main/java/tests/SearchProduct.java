package tests;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import core.BaseTest;

public class SearchProduct extends BaseTest {

    @Test
    public void searchProduct() {
        homePage.typeSearchTextBox("selenium webdriver book");
        homePage.clickSubmit();
        assertThat(searchResultsPage.getResultsFor(), containsString("selenium webdriver book"));
    }

}