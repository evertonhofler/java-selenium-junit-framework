package pages;

import static core.Properties.interactiveWait;

import org.openqa.selenium.By;

import core.BasePage;

public class HomePage extends BasePage {
    
    public HomePage typeSearchTextBox(String searchKeywords){
        interactiveWait();
        type("twotabsearchtextbox", searchKeywords);
        return this;
    }

    public SearchResultsPage clickSubmit(){
        interactiveWait();
        click(By.cssSelector("div.nav-right > div > input"));
        return new pages.SearchResultsPage();
    }

}