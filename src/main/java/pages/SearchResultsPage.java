package pages;

import static core.Properties.interactiveWait;

import org.openqa.selenium.By;

import core.BasePage;

public class SearchResultsPage extends BasePage {

    public String getResultsFor () {
        interactiveWait();
        return getText(By.cssSelector("span#s-result-count span > span"));
    }

}