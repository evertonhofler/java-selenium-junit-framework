package support;

import static core.DriverFactory.getDriver;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.DriverFactory;
import core.Properties;

public class Utilities {

    public static void takeScreenshot(String fileName) throws IOException {
        TakesScreenshot ss = (TakesScreenshot) getDriver();
        File file = ss.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target" + File.separator + "screenshot" + 
        		File.separator + Properties.SUITE_START_TIMESTAMP + File.separator + fileName + ".jpg"));
    }
    
    public static void highlightField(By by) {
    	WebDriver driver = DriverFactory.getDriver();        
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	WebElement element = driver.findElement(by);
    	js.executeScript("arguments[0].style.border = arguments[1]", element, "solid 4px red");
    }

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
           .format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS"));
    }
    
}